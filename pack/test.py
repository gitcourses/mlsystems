import bottle
import json
from bottle_tools import fill_args


app = bottle.Bottle()
with open("test.json", "r") as fl:
    public = json.load(fl)
n_private = 1000
private = public["samples"][-n_private:]
public = public["samples"][:-n_private]
public_pre = [i for i, _ in public]
public_post = [j for _, j in public]


@app.get("/test")
def send_data():
    return {"samples": public_pre}


@app.post("/test/public")
def test_data(samples):
    return {
        "em": sum(
            exp == given
            for exp, given in zip(public_post, samples)
            if given is not None
        )
        / len(public_post)
    }


@app.post("/test/private")
def test_data(samples):
    return {
        "em": sum(
            exp == given
            for exp, given in zip(private_post, samples)
            if given is not None
        )
        / len(public_post)
    }
